package utilities;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import io.restassured.specification.RequestSpecification;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
public class ApisExtensions {
    public static RequestSpecification request;
    public ApisExtensions() {
//      configuration of rest assured to used at API requests
        
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri("");
        builder.setContentType(ContentType.JSON);
        var requestSpec = builder.build();
        request = RestAssured.given().spec(requestSpec);
    }

    public static ResponseOptions<Response> GetResponseWithParams(String url, Map<String, String> queryParams) {
        request.queryParams(queryParams);
        try {
            return request.get(new URI(url));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }
}
