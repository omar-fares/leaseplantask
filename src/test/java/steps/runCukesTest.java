package steps;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import java.time.LocalDateTime;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty",
                "html:reports"
        },
        features = {"src/test/java/features"},
        glue = {"steps"}
)
class RunCukesTest{}
