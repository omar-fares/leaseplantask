$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/features/Features.feature");
formatter.feature({
  "name": "check Cucumber",
  "description": "  get country city weather and country capital name",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "get correct city weather",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to get weather for \"Cairo\" country",
  "keyword": "Given "
});
formatter.match({
  "location": "steps.weathersteps.iWantToGetWeatherForCountryAt(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "be sure that the returned weather is related to \"Cairo\"",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.weathersteps.beSureThatTheReturnedCountryIs(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "get All Countries Data",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to get countries Data",
  "keyword": "Given "
});
formatter.match({
  "location": "steps.countrysteps.iWantToGetCountriesData()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "if the country is \"Egypt\"",
  "keyword": "When "
});
formatter.match({
  "location": "steps.countrysteps.ifTheCountryIs(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the capital will be \"Cairo\"",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.countrysteps.theCapitalWillBe(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
});