package steps;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import org.junit.Assert;
import utilities.ApisExtensions;
import java.util.HashMap;
import java.util.Map;

public class weathersteps{

    private static ResponseOptions<Response> response;
//        the url for the API
    private static String URL = "http://api.weatherstack.com/current";

    @Given("I want to get weather for {string} city")
    public void iWantToGetWeatherForCountryAt(String Country) {
//      hashMap to insert the query parameters as key and value
        Map<String, String> queryParams = new HashMap<>();
//      set "access_key" param to query parameter hash map
        queryParams.put("access_key","c35d583d02122a032c530605e079a824");
//      set "query" param to query parameter hash map
        queryParams.put("query",Country);
//      send the parameter of request and receive the response at ResponseOptions object
        response = ApisExtensions.GetResponseWithParams(URL ,queryParams);
    }

    @Then("be sure that the returned weather is related to {string}")
    public void beSureThatTheReturnedCountryIs(String Country) {
//      get the value of key "location.name"
        String name = response.getBody().jsonPath().get("location.name").toString();
//        assert that the returned data is like the expected data
        Assert.assertEquals("unfortunatly, the Country not same as needed",name, Country);
    }
}
