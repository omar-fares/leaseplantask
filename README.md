**Lease Plan Maven Task:**

Test Rest API using Cucumber testing framework

**Project Name:**

Lease Plan Task

**Project Description:**

It has 2 scenarios written with gherkin.

1. first scenario to test weather API (http://api.weatherstack.com/current) which return the weather for the country which already sent in query parameter.
2. second scenario to test the countries data in 2 phases:
- It call all countries API (https://restcountries.eu/rest/v2/All) to check that API work fine by checking its Status code.
- It call specific country name API(https://restcountries.eu/rest/v2/name/{Country name}) to check that the returned country data include Capital attribute which check that its the same as sent at the scenario 


The Code is uploaded to GitLab (GitLab is a web-based DevOps lifecycle tool that provides a Git repository manager providing wiki)
and connected with pipeline CI/CD to trigger 3 phases once there is any push command run to the repository.
- clean phase to clean the code 
- build phase to compile and build the project
- test phase to run the scenarios and get the results

**the pipeline connected to shared GitLab runner to Act as always working Server to run this phases**

**Installation:**

**Pre-requisite** You need to have following softwares installed on your computer
1. Install JDK 1.8 or above and set path
2. Install Maven and set path
3. Eclipse or Intellij
4. Plugins: Maven and Cucumber

You can clone this repository to run all test cases locally by : git clone git@gitlab.com:omar-fares/leaseplantask.git
then open the CMD to run the scenarios by the below steps:
1. cd {project Directory}
2. mvn clean 
3. mvn build
4. mvn -fn test ( -fn used to continue all tests even there is one failed )
> the below step is just work locally not at pipeline
5. mvn surefire-report:report-only site -DgenerateReports=false

After that you can go to {project directory}/target/site and you will find the report for the scenarios run 

If you need to change any scenario attributes or add new scenarios just go to file Features.feature at path src/test/java/features and to add new step definition you can write it at already exist file or create new one at steps package at path src/test/java/steps **OR** try to stop at every annotation at scenario (given - when - then) press alt + enter then click add new step definition 
