Feature: check Cucumber
  get country city weather and country capital name

#  first scenario to test the weather of specific city
  Scenario: get correct city weather
    Given I want to get weather for "Cairo" city
    Then be sure that the returned weather is related to "Caro"

#  second scenario to test the country data and be sure that the capital is correct
  Scenario: get All Countries Data
    Given I want to get countries Data
    When if the country is "Egypt"
    Then the capital will be "Cairo"
