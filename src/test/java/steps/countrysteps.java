package steps;

import org.json.JSONArray;
import org.json.JSONObject;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import org.junit.Assert;
import utilities.ApisExtensions;

import java.util.HashMap;
import java.util.Map;

public class countrysteps {

    private static ResponseOptions<Response> response;
//    the url for the API
    private static String URL = "https://restcountries.eu/rest/v2/";

    @Given("I want to get countries Data")
    public void iWantToGetCountriesData() {
//        hashMap to insert the query parameters as key and value
        Map<String, String> queryParams = new HashMap<>();
//        send the parameter of request and receive the response at ResponseOptions object
        response = ApisExtensions.GetResponseWithParams(URL ,queryParams);
//        assert that the returned data is like the expected data
        Assert.assertEquals("unfortunatly, Some thing went wrong",200, response.getStatusCode());
    }

    @When("if the country is {string}")
    public void ifTheCountryIs(String Country) {
//        choose specific country to view its data
        URL = URL + "name/" + Country;
//        hashMap to insert the query parameters as key and value
        Map<String, String> queryParams = new HashMap<>();
//        send the parameter of request and receive the response at ResponseOptions object
        response = ApisExtensions.GetResponseWithParams(URL ,queryParams);
    }

    @Then("the capital will be {string}")
    public void theCapitalWillBe(String City) {
//      parse response to string
        String CountryData = response.getBody().asString();
//      parse string to JSONArray
        JSONArray CountryArray = new JSONArray(CountryData);
//      get the index[0] of the array
        final JSONObject country = CountryArray.getJSONObject(0);
//      assert that the returned data is like the expected data
        Assert.assertEquals("unfortunatly, this Country capital not "+City ,country.getString("capital"), City);
    }
}
