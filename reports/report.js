$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/features/Features.feature");
formatter.feature({
  "name": "check Cucumber",
  "description": "  get country city weather and country capital name",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "get correct city weather",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to get weather for \"Cairo\" city",
  "keyword": "Given "
});
formatter.match({
  "location": "steps.weathersteps.iWantToGetWeatherForCountryAt(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "be sure that the returned weather is related to \"Caro\"",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.weathersteps.beSureThatTheReturnedCountryIs(java.lang.String)"
});
formatter.result({
  "error_message": "org.junit.ComparisonFailure: unfortunatly, the Country not same as needed expected:\u003cCa[i]ro\u003e but was:\u003cCa[]ro\u003e\n\tat org.junit.Assert.assertEquals(Assert.java:117)\n\tat steps.weathersteps.beSureThatTheReturnedCountryIs(weathersteps.java:34)\n\tat ✽.be sure that the returned weather is related to \"Caro\"(file:///Users/omarfares/IdeaProjects/TestAssured/src/test/java/features/Features.feature:7)\n",
  "status": "failed"
});
formatter.scenario({
  "name": "get All Countries Data",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to get countries Data",
  "keyword": "Given "
});
formatter.match({
  "location": "steps.countrysteps.iWantToGetCountriesData()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "if the country is \"Egypt\"",
  "keyword": "When "
});
formatter.match({
  "location": "steps.countrysteps.ifTheCountryIs(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the capital will be \"Cairo\"",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.countrysteps.theCapitalWillBe(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
});